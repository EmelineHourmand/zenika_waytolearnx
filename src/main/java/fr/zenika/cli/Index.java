package fr.zenika.cli;

import fr.zenika.cli.screen.HomeScreen;
import fr.zenika.cli.screen.Screen;
import fr.zenika.domaine.GlobalMcq;
import fr.zenika.domaine.service.McqFactory;
import fr.zenika.domaine.Score;
import fr.zenika.domaine.ScoreList;
import fr.zenika.domaine.service.GlobalMcqManager;
import fr.zenika.domaine.service.ScoreManager;

import java.util.ArrayList;

/**
 * @author Emeline Hourmand
 */
public class Index {

    /**
     * Méthode d'entré de l'application.
     * @param args arguments passés en paramètre.
     */
    public static void main(String[] args) {

        // Initialisation de l'instance globalMcq
        GlobalMcq globalMcq = GlobalMcqManager.getInstance().getGlobalMcq();
        globalMcq.getMcq().add(McqFactory.initMcq1());
        globalMcq.getMcq().add(McqFactory.initMcq2());

        // Initialisation de la liste de score.
        ScoreList scoreList = ScoreManager.getINSTANCE().getScore();
        scoreList.addScoreToScoreList(new Score(1, new ArrayList<>()));
        scoreList.addScoreToScoreList(new Score(2, new ArrayList<>()));

        // Envois le premier écran.
        Screen.displayFirstScreen(new HomeScreen());

    }
}
