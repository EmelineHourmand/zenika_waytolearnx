package fr.zenika.cli.screen;

/**
 * @author Emeline Hourmand
 */
public class HomeScreen implements Screen {

    /**
     * Initialise l'application.
     * @return au menu principal.
     */
    @Override
    public Screen render() {
        System.out.println("=== QCM WayToLearnX ===");

        return new MainMenuScreen();
    }
}