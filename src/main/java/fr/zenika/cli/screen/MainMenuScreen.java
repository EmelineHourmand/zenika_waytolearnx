package fr.zenika.cli.screen;

import fr.zenika.cli.ScannerManager;
import fr.zenika.domaine.GlobalMcq;
import fr.zenika.domaine.Mcq;
import fr.zenika.domaine.service.GlobalMcqManager;

/**
 * @author Emeline Hourmand
 */
public class MainMenuScreen implements Screen {

    GlobalMcq globalMcq = GlobalMcqManager.getInstance().getGlobalMcq();
    Mcq mcqOop = globalMcq.getMcq().get(0);
    Mcq mcqCollectionP1 = globalMcq.getMcq().get(1);

    /**
     * Permet d'afficher le menu principal et de rediriger le client en fonction de son choix.
     * @return un écran en fonction du choix du client.
     */
    @Override
    public Screen render() {

        System.out.println("""
                Quel QCM souhaitez vous réalisez ?
                    1 - Java - Programmation orientée objet
                    2 - Java - Les collections - partie 1
                    3 - Tableau des scores
                    4 - Quitter l’application
               
                """);

        int choice = ScannerManager.getInstance().getCustomerChoiceInteger();

        return switch (choice) {
            case 1 -> new McqScreen(mcqOop);
            case 2 -> new McqScreen(mcqCollectionP1);
            case 3 -> new ScoreMenuScreen();
            case 4 -> new FinishScreen();
            default -> this;
        };
    }
}
