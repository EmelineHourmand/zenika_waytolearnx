package fr.zenika.cli.screen;

import fr.zenika.cli.ScannerManager;
import fr.zenika.domaine.*;
import fr.zenika.domaine.service.ScoreManager;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Emeline Hourmand
 */
public class McqScreen implements Screen {

    private Mcq mcq;

    /**
     * Constructeur de QCM.
     * @param mcq un QCM.
     */
    public McqScreen(Mcq mcq) {
        this.mcq = mcq;
    }

    /**
     * Permet l'affichage d'un QCM.
     * @return au menu principal.
     */
    @Override
    public Screen render() {

        User user = new User("", 0, new ArrayList<>(), new ArrayList<>(), LocalDateTime.now());
        ScoreList scoreList = ScoreManager.getINSTANCE().getScore();
        Score score = new Score();

        System.out.println(mcq.getMcqName());
        System.out.println();

        for (Question question : mcq.getQuestions()) {
            System.out.println("= Question " + question.getQuestionId());
            System.out.println(question.getQuestionLabel());
            System.out.println();

            for (Answer answer : question.getAnswers()) {
                System.out.println(answer.getAnswerId() + " - " + answer.getAnswerLabel());
            }

            System.out.println();
            System.out.println("Votre réponse : ");
            String choice = ScannerManager.getInstance().getCustomerChoiceString().toLowerCase();

            boolean isFalse = true;
            Set<String> userAnswer = new HashSet<>();
            userAnswer.add(choice);

            if (userAnswer.containsAll(question.getGreatAnswer())) {
                isFalse = false;
            }

            if (!isFalse) {
                user.setScore(user.getScore() + 1);
            } else {
                user.setFalseQuestion(question);
                user.setFalseAnswer(choice);
            }
        }

        System.out.println();
        System.out.println("= Résultat du QCM");
        if (user.getScore() != mcq.getQuestions().size()) {
            System.out.println("Votre note est de " + user.getScore() + "/" + mcq.getQuestions().size());
            System.out.println("Voulez-vous voir les réponses des questions vous avez échouées (Y/N) ?");

            String choice = ScannerManager.getInstance().getCustomerChoiceString();
            if (choice.equals("y")) {
                System.out.println("= Réponses du QCM");
                List<Question> falseQuestions = user.getFalseQuestion();
                List<String> falseAnswers = user.getFalseAnswer();
                for (int y = 0; y < falseQuestions.size(); y++) {
                    Question question = falseQuestions.get(y);
                    String falseAnswer = falseAnswers.get(y);
                    System.out.println("= Question " + question.getQuestionId());
                    System.out.println(question.getQuestionLabel());

                    for (Answer answer : question.getAnswers()) {
                        System.out.println(answer.getAnswerId() + " - " + answer.getAnswerLabel());

                    }
                    StringBuilder goodAnswer = new StringBuilder();
                    for (String greatAnswer :
                            question.getGreatAnswer()) {
                        goodAnswer.append(greatAnswer).append(" ");
                    }

                    System.out.println("Votre réponse : " + falseAnswer.toUpperCase());
                    System.out.println("Réponse correcte : " + goodAnswer + "\n");
                    System.out.println("Explications : " + question.getAnswerDetails() + "\n");

                }
            }

        } else {
            System.out.println("Bravo ! Vous avez fait un sans faute et obtenu un "
                    + user.getScore() + "/" + mcq.getQuestions().size());
        }

        System.out.println("Quel est votre nom ?");

        String name = ScannerManager.getInstance().getCustomerChoiceString();
        user.setName(name);
        score.setUsers(user);
        scoreList.addScoreToScoreList(score);

        System.out.println();
        System.out.println("=== Retour au menu principal. ===");
        return new MainMenuScreen();

    }
}

