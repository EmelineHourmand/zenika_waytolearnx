package fr.zenika.cli.screen;

import fr.zenika.domaine.GlobalMcq;
import fr.zenika.domaine.Mcq;
import fr.zenika.domaine.Score;
import fr.zenika.domaine.User;
import fr.zenika.domaine.service.GlobalMcqManager;

/**
 * @author Emeline Hourmand
 */
public class ScoreScreen implements Screen {

    private Score score;

    /**
     * Constructeur de score.
     * @param score un score.
     */
    public ScoreScreen(Score score) {
        this.score = score;
    }

    /**
     * Gère l'écran d'affichage des scores en fonction du questionnaire.
     * @return au menu des scores.
     */
    @Override
    public Screen render() {

        score.updateUserList();

        GlobalMcq globalMcq = GlobalMcqManager.getInstance().getGlobalMcq();
        Mcq mcq = globalMcq.getMcq().get(score.getScoreId());

        switch (score.getScoreId()) {
            case 1 -> System.out.println("= Tableau des scores du QCM Java - Programmation orientée objet");
            case 2 -> System.out.println("= Tableau des scores du QCM Java - Les collections - partie 1");
            default -> new MainMenuScreen();
        }

        System.out.println("#   Nom       Score      Date");
        int id = 0;
        for (int i = 0; i <= score.getUsers().size(); i++) {
            if (i <= score.getUsers().size()-1 && i <= 4) {
                User user = score.getUsers().get(i);
                id += 1;
                System.out.println(id + " - " + user.getName() + "       "
                        + user.getScore()+ "/" + mcq.getQuestions().size() + "       " + user.getDateMcq());
            } else {
                break;
            }
        }
        System.out.println();

        return new ScoreMenuScreen();
    }
}
