package fr.zenika.cli.screen;

/**
 * @author Emeline Hourmand
 */
public class FinishScreen implements Screen {

    /**
     * Afficher l'écran de fin de l'application.
     * @return null pour quitter l'application.
     */
    @Override
    public Screen render() {
        System.out.println("""
                
                === FIN DU PROGRAMME : WayToLearnX ===
                    A bientôt !
                """);
        return null;
    }
}
