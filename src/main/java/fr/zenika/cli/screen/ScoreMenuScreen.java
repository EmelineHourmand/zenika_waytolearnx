package fr.zenika.cli.screen;

import fr.zenika.cli.ScannerManager;
import fr.zenika.domaine.Score;
import fr.zenika.domaine.ScoreList;
import fr.zenika.domaine.service.ScoreManager;

public class ScoreMenuScreen implements Screen {

    ScoreList scoreList = ScoreManager.getINSTANCE().getScore();
    Score scoreOop = scoreList.getScores().get(0);
    Score scoreCollectionP1 = scoreList.getScores().get(1);

    @Override
    public Screen render() {

        System.out.println("""
                = Tableaux des scores
                Quel QCM souhaitez-vous consulter le tableau des scores ?
                    1 - Java - Programmation orientée objet
                    2 - Java - Les collections - partie 1
                    3 - Retour au menu principal.
                """);

        int choice = ScannerManager.getInstance().getCustomerChoiceInteger();

        return switch (choice) {
            case 1 -> new ScoreScreen(scoreOop);
            case 2 -> new ScoreScreen(scoreCollectionP1);
            case 3 -> new MainMenuScreen();
            default -> this;
        };
    }
}
