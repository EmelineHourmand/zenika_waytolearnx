package fr.zenika.domaine;

import java.util.List;
import java.util.Set;

/**
 * @author Emeline Hourmand
 */
public class Question {

    private final int questionId;
    private final String questionLabel;
    private final List<Answer> answers;
    private final Set<String> greatAnswer;
    private final String answerDetails;

    /**
     * Constructeur de question.
     * @param questionId l'ID de la question.
     * @param questionLabel le texte de la question.
     * @param answerList les réponses associées à la question.
     */
    public Question(int questionId, String questionLabel, List<Answer> answerList, Set<String> greatAnswer, String answerDetails) {
        this.questionId = questionId;
        this.questionLabel = questionLabel;
        this.answers = answerList;
        this.greatAnswer = greatAnswer;
        this.answerDetails = answerDetails;
    }

    /**
     * Récupère l'ID de la question.
     * @return l'ID de la question.
     */
    public int getQuestionId() {
        return questionId;
    }

    /**
     * Récupère le texte de la question.
     * @return le texte de la question.
     */
    public String getQuestionLabel() {
        return questionLabel;
    }

    /**
     * Retourne la liste des réponses de la question.
     * @return la liste des réponses de la question.
     */
    public List<Answer> getAnswers() {
        return answers;
    }

    /**
     * Retourne la liste des bonnes réponses de la question.
     * @return la liste des bonnes réponses de la question.
     */
    public Set<String> getGreatAnswer() {
        return greatAnswer;
    }

    /**
     * Retourne les détails de la réponse.
     * @return retourne les détails de la réponse.
     */
    public String getAnswerDetails() {
        return answerDetails;
    }
}
