package fr.zenika.domaine;

import java.util.Comparator;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class Score {

    private int scoreId;
    private List<User> users;

    /**
     * Constructeur de Score.
     * @param scoreId l'id du score.
     * @param users liste d'utilisateurs.
     */
    public Score(int scoreId, List<User> users) {
        this.scoreId = scoreId;
        this.users = users;
    }

    /**
     * Constructeur vide de score.
     */
    public Score() {
    }

    /**
     * Permet de récupérer l'id du score.
     * @return l'id du score.
     */
    public int getScoreId() {
        return scoreId;
    }

    /**
     * Permet de renseigner l'id du score.
     * @param scoreId un id.
     */
    public void setScoreId(int scoreId) {
        this.scoreId = scoreId;
    }

    /**
     * Permet de récupérer la liste des utilisateurs ayant fait un score.
     * @return la liste des utilisateurs ayant fait un score.
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Permet d'ajouter un utilisateur a la liste des utilisateurs ayant fait un score.
     * @param user un utilisateur.
     */
    public void setUsers(User user) {
        this.users.add(user);
    }

    /**
     * Permet de trier la liste des utilisateurs en fonction de leurs scores.
     * La liste est triée par ordre décroissant.
     */
    public void updateUserList() {
        for (int i = 0; i < users.size(); i++) {
            users.sort(Comparator.comparingInt(User::getScore).reversed());
        }
    }
}
