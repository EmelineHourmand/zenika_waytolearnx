package fr.zenika.domaine;

import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class Mcq {

    private final int mcqId;
    private final String mcqName;
    private final List<Question> questions;

    /**
     * Constructeur de QCM.
     * @param mcqId l'ID du QCM.
     * @param questions les questions du QCM
     */
    public Mcq(int mcqId, String mcqName, List<Question> questions) {
        this.mcqId = mcqId;
        this.mcqName = mcqName;
        this.questions = questions;
    }


    /**
     * Récupère l'ID du QCM.
     * @return l'ID du QCM.
     */
    public int getMcqId() {
        return mcqId;
    }

    /**
     * Récupère les questions du QCM.
     * @return les questions du QCM.
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * Retourne le nom du QCM.
     * @return le nom du QCM.
     */
    public String getMcqName() {
        return mcqName;
    }
}
