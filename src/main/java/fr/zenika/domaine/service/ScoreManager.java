package fr.zenika.domaine.service;

import fr.zenika.domaine.ScoreList;

/**
 * @author Emeline Hourmand
 */
public class ScoreManager {

    private static final ScoreManager INSTANCE = new ScoreManager();
    private static final ScoreList scores = new ScoreList();

    /**
     * Constructeur privé pour interdire la création d'un nouvel GlobalMcq.
     */
    private ScoreManager() {
    }

    /**
     * Récupère l'instance de ScoreManager.
     * @return l'instance de ScoreManager.
     */
    public static ScoreManager getINSTANCE() {
        return INSTANCE;
    }

    /**
     * Récupère ScoreList.
     * @return ScoreList.
     */
    public ScoreList getScore() {
        return scores;
    }
}
