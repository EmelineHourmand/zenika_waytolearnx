package fr.zenika.domaine.service;

import fr.zenika.domaine.GlobalMcq;

/**
 * @author Emeline Hourmand
 */
public class GlobalMcqManager {

    private static final GlobalMcq globalMcq = new GlobalMcq();
    private static final GlobalMcqManager INSTANCE = new GlobalMcqManager();

    /**
     * Constructeur privé pour interdire la création d'un nouvel GlobalMcq.
     */
    private GlobalMcqManager() {
    }

    /**
     * Récupère l'instance de GlobaleMcqManager.
     * @return l'instance de GlobaleMcqManager.
     */
    public static GlobalMcqManager getInstance() {
        return INSTANCE;
    }

    /**
     * Récupère GlobalMcq.
     * @return GlobalMcq.
     */
    public GlobalMcq getGlobalMcq() {
        return globalMcq;
    }
}
