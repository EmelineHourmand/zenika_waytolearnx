package fr.zenika.domaine.service;

import fr.zenika.domaine.Answer;
import fr.zenika.domaine.Mcq;
import fr.zenika.domaine.Question;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author Emeline Hourmand
 */
@SuppressWarnings("SuppressWarning")
public class McqFactory {

    /**
     * Initialisation du QCM 1.
     * @return le QCM 1.
     */
    public static Mcq initMcq1() {

        List<Answer> answersQuestion1 = List.of(
                new Answer('A', "Héritage"),
                new Answer('B', "Encapsulation"),
                new Answer('C', "Polymorphisme"),
                new Answer('D', "Compilation")
        );
        String questionLabel1 = "Lequel des éléments suivants n’est pas un concept POO en Java?";
        Set<String> greatAnswer1 = new HashSet<>();
        greatAnswer1.add("D");
        String answerDetail1 = "Il existe 4 concepts POO en Java. Héritage,\n" +
                "encapsulation, polymorphisme et abstraction.";

        List<Answer> answersQuestion2 = List.of(
                new Answer('A', "Au moment de l'exécution"),
                new Answer('B', "Au moment de la compilation"),
                new Answer('C', "Au moment du codage"),
                new Answer('D', "Au moment de l’exécution")
        );
        String questionLabel2 = "Quand la surcharge de méthode est-elle déterminée?";
        Set<String> greatAnswer2 = new HashSet<>();
        greatAnswer2.add("B");
        String answerDetail2 = "La surcharge est déterminée au moment de la compilation.";

        List<Answer> answersQuestion3 = List.of(
                new Answer('A', "Quand il y a plusieurs méthodes avec le même nom mais avec une\n" +
                        "signature de méthode différente et un nombre ou un type de paramètres différent"),
                new Answer('B', "Quand il y a plusieurs méthodes avec le même nom, le même nombre\n" +
                        "de paramètres et le type mais une signature différente"),
                new Answer('C', "Quand il y a plusieurs méthodes avec le même nom, la même signature,\n" +
                        "le même nombre de paramètres mais un type différent"),
                new Answer('D', "Quand il y a plusieurs méthodes avec le même nom, la même signature\n" +
                        "mais avec différente signature")
        );
        String questionLabel3 = "Quand la surcharge ne se produit pas?";
        Set<String> greatAnswer3 = new HashSet<>();
        greatAnswer3.add("B");
        String answerDetail3 = "La surcharge survient lorsque plusieurs méthodes ont le même nom mais un constructeur\n" +
                "différent et aussi quand la même signature mais un nombre différent de paramètres et / ou de type de paramètre.";

        List<Answer> answersQuestion4 = List.of(
                new Answer('A', "Polymorphisme"),
                new Answer('B', "Encapsulation"),
                new Answer('C', "Abstraction"),
                new Answer('D', "Héritage")
        );
        String questionLabel4 = "Quel concept de Java est un moyen de convertir des objets du monde réel en termes de classe?";
        Set<String> greatAnswer4 = new HashSet<>();
        greatAnswer4.add("C");
        String answerDetail4 = "L’abstraction est un concept de définition des objets du monde réel en termes de classes ou d’interfaces.";

        List<Answer> answersQuestion5 = List.of(
                new Answer('A', "Polymorphisme"),
                new Answer('B', "Encapsulation"),
                new Answer('C', "Abstraction"),
                new Answer('D', "Héritage")
        );
        String questionLabel5 = "Quel concept de Java est utilisé en combinant des méthodes et des attributs dans une classe?";
        Set<String> greatAnswer5 = new HashSet<>();
        greatAnswer5.add("B");
        String answerDetail5 = "L’encapsulation est implémentée en combinant des méthodes et des attributs dans une \n" +
                "classe. La classe agit comme un conteneur de propriétés d’encapsulation.";

        List<Answer> answersQuestion6 = List.of(
                new Answer('A', "Agrégation"),
                new Answer('B', "Composition"),
                new Answer('C', "Encapsulation"),
                new Answer('D', "Association")
        );
        String questionLabel6 = "Comment ça s’appelle si un objet a son propre cycle de vie?";
        Set<String> greatAnswer6 = new HashSet<>();
        greatAnswer6.add("D");
        String answerDetail6 = "C’est une relation où tous les objets ont leur propre cycle de vie. Cela se produit\n" +
                " lorsque de nombreuses relations sont disponibles, au lieu de One To One ou One To Many.";

        List<Answer> answersQuestion7 = List.of(
                new Answer('A', "Agrégation"),
                new Answer('B', "Composition"),
                new Answer('C', "Encapsulation"),
                new Answer('D', "Association")
        );
        String questionLabel7 = "Comment s’appelle-t-on dans le cas où l’objet d’une classe mère est détruit donc \n" +
                "l’objet d’une classe fille sera détruit également?";
        Set<String> greatAnswer7 = new HashSet<>();
        greatAnswer7.add("B");
        String answerDetail7 = "La composition se produit lorsque l’objet d’une classe fille est détruit si l’objet \n" +
                "de la classe mère est détruit. La composition est également appelée une agrégation forte.";

        List<Answer> answersQuestion8 = List.of(
                new Answer('A', "Agrégation"),
                new Answer('B', "Composition"),
                new Answer('C', "Encapsulation"),
                new Answer('D', "Association")
        );
        String questionLabel8 = "Comment s’appelle-t-on l’objet a son propre cycle de vie et l’objet d’une classe\n" +
                " fille ne dépend pas à un autre objet d’une classe mère?";
        Set<String> greatAnswer8 = new HashSet<>();
        greatAnswer8.add("A");
        String answerDetail8 = "L’agrégation se produit lorsque les objets ont leur propre cycle de vie et que \n" +
                " l’objet d’une classe fille peut être associé à un seul objet d’une classe mère.";

        List<Answer> answersQuestion9 = List.of(
                new Answer('A', "Vrais"),
                new Answer('B', "Faux")
               );
        String questionLabel9 = "La surcharge d’une méthode peut remplacer l’héritage et le polymorphisme?";
        Set<String> greatAnswer9 = new HashSet<>();
        greatAnswer9.add("A");
        String answerDetail9 = "L’agrégation se produit lorsque les objets ont leur propre cycle de vie et que \n" +
                "l’objet d’une classe fille peut être associé à un seul objet d’une classe mère.3";

        List<Answer> answersQuestion10 = List.of(
                new Answer('A', "final"),
                new Answer('B', "private"),
                new Answer('C', "abstract"),
                new Answer('D', "protected"),
                new Answer('E', "public")
        );
        String questionLabel10 = "Quels keywords sont utilisés pour spécifier la visibilité des propriétés et des méthodes ?";
        Set<String> greatAnswer10 = new HashSet<>();
        greatAnswer10.add("B");
        greatAnswer10.add("D");
        greatAnswer10.add("E");
        String answerDetail10 = "les mots clés utilisés pour spécifier la visibilité des propriétés et des méthodes sont : \n" +
                "private, protected, public.";
        //////

        List<Question> questionsMcq1 = List.of(
                new Question(1, questionLabel1, answersQuestion1, greatAnswer1, answerDetail1),
                new Question(2, questionLabel2, answersQuestion2, greatAnswer2, answerDetail2),
                new Question(3, questionLabel3, answersQuestion3, greatAnswer3, answerDetail3),
                new Question(4, questionLabel4, answersQuestion4, greatAnswer4, answerDetail4),
                new Question(5, questionLabel5, answersQuestion5, greatAnswer5, answerDetail5),
                new Question(6, questionLabel6, answersQuestion6, greatAnswer6, answerDetail6),
                new Question(7, questionLabel7, answersQuestion7, greatAnswer7, answerDetail7),
                new Question(8, questionLabel8, answersQuestion8, greatAnswer8, answerDetail8),
                new Question(9, questionLabel9, answersQuestion9, greatAnswer9, answerDetail9),
                new Question(10, questionLabel10, answersQuestion10, greatAnswer10, answerDetail10)
        );

        String mcqName = "=== QCM Java - Programmation orientée objet";

        return new Mcq(1, mcqName, questionsMcq1);
    }

    /**
     * Initialisation du QCM 2.
     * @return le QCM 2.
     */
    public static Mcq initMcq2() {

        List<Answer> answersQuestion1 = List.of(
                new Answer('A', "java.awt"),
                new Answer('B', "java.net"),
                new Answer('C', "java.util"),
                new Answer('D', "java.lang")
        );
        String questionLabel1 = "Lequel de ces packages contient toutes les classes de collection?";
        Set<String> greatAnswer1 = new HashSet<>();
        greatAnswer1.add("C");
        String answerDetail1 = "Les classes du Framework Collections se trouvent dans le package java.util.";

        List<Answer> answersQuestion2 = List.of(
                new Answer('A', "Queue"),
                new Answer('B', "Stack"),
                new Answer('C', "Array"),
                new Answer('D', "Map")
        );
        String questionLabel2 = "Laquelle de ces classes ne fait pas partie du framework collection en Java?";
        Set<String> greatAnswer2 = new HashSet<>();
        greatAnswer2.add("D");
        String answerDetail2 = "La classe Map ne fait pas partie du framework collection.";

        List<Answer> answersQuestion3 = List.of(
                new Answer('A', "SortedMap"),
                new Answer('B', "SortedList"),
                new Answer('C', "Set"),
                new Answer('D', "List")
        );
        String questionLabel3 = "Laquelle de ces interfaces ne fait pas partie du framework collection en Java?";
        Set<String> greatAnswer3 = new HashSet<>();
        greatAnswer3.add("B");
        String answerDetail3 = "L’interface SortedList ne fait pas partie du framework collection.";

        List<Answer> answersQuestion4 = List.of(
                new Answer('A', "refresh()"),
                new Answer('B', "delete()"),
                new Answer('C', "reset()"),
                new Answer('D', "clear()")
        );
        String questionLabel4 = "Laquelle de ces méthodes supprime tous les éléments d’une collection?";
        Set<String> greatAnswer4 = new HashSet<>();
        greatAnswer4.add("D");
        String answerDetail4 = "La méthode clear() supprime tous les éléments d’une collection.";

        List<Answer> answersQuestion5 = List.of(
                new Answer('A', "Un groupe d’objets"),
                new Answer('B', "Un groupe d’interfaces"),
                new Answer('C', "Un groupe de classes"),
                new Answer('D', "Aucune de ces réponses n’est vraie.")
        );
        String questionLabel5 = "Qu’est-ce que Collection en Java?";
        Set<String> greatAnswer5 = new HashSet<>();
        greatAnswer5.add("A");
        String answerDetail5 = "Une collection est un objet qui représente un groupe d’objets.";

        List<Answer> answersQuestion6 = List.of(
                new Answer('A', "Collection"),
                new Answer('B', "Set"),
                new Answer('C', "Group"),
                new Answer('D', "List")
        );
        String questionLabel6 = "Laquelle de ces interfaces ne fait pas partie du framework collection en Java?";
        Set<String> greatAnswer6 = new HashSet<>();
        greatAnswer6.add("C");
        String answerDetail6 = "Group ne fait pas partie du framework collection.";

        List<Answer> answersQuestion7 = List.of(
                new Answer('A', "Set"),
                new Answer('B', "List"),
                new Answer('C', "Map"),
                new Answer('D', "Tout les réponses sont vrais")
        );
        String questionLabel7 = "Quelle interface n’autorise pas les doublons?";
        Set<String> greatAnswer7 = new HashSet<>();
        greatAnswer7.add("A");
        String answerDetail7 = "Voir : Différence entre List, Set et Map en java.";

        List<Answer> answersQuestion8 = List.of(
                new Answer('A', "Array"),
                new Answer('B', "Arrays"),
                new Answer('C', "ArrayList"),
                new Answer('D', "Tout les réponses sont vrais")
        );
        String questionLabel8 = "Laquelle de ces classes de collection a la capacité d’évoluer de façon dynamique?";
        Set<String> greatAnswer8 = new HashSet<>();
        greatAnswer8.add("C");
        String answerDetail8 = "ArrayList est un tableau redimensionnable qui implémente l’interface List.";

        List<Answer> answersQuestion9 = List.of(
                new Answer('A', "des valeurs nulles"),
                new Answer('B', "une clé nulle"),
                new Answer('C', "Tout les réponses sont vrais"),
                new Answer('D', "Aucune de ces réponses n’est vraie.")
        );
        String questionLabel9 = "Un HashMap autorise _____________";
        Set<String> greatAnswer9 = new HashSet<>();
        greatAnswer9.add("C");
        String answerDetail9 = "HashMap autorise une clé nulle et les valeurs nulles (une seule clé nulle est autorisée\n" +
                " car deux clés ne sont pas autorisées). Par contre Hashtable n’autorise pas les clés nulles ou les valeurs nulles.";

        List<Answer> answersQuestion10 = List.of(
                new Answer('A', "La redéfinition de la méthode equals"),
                new Answer('B', "La redéfinition de la méthode hashCode"),
                new Answer('C', "Tout les réponses sont vrais"),
                new Answer('D', "Aucune de ces réponses n’est vraie.")
        );
        String questionLabel10 = "L’efficacité d’un HashMap peut être garantie avec __________";
        Set<String> greatAnswer10 = new HashSet<>();
        greatAnswer10.add("C");
        String answerDetail10 = "HashMap s’appuie sur la méthode equals() et hashCode() pour comparer les clés et les valeurs.";

        //////

        List<Question> questionsMcq2 = List.of(
                new Question(1, questionLabel1, answersQuestion1, greatAnswer1, answerDetail1),
                new Question(2, questionLabel2, answersQuestion2, greatAnswer2, answerDetail2),
                new Question(3, questionLabel3, answersQuestion3, greatAnswer3, answerDetail3),
                new Question(4, questionLabel4, answersQuestion4, greatAnswer4, answerDetail4),
                new Question(5, questionLabel5, answersQuestion5, greatAnswer5, answerDetail5),
                new Question(6, questionLabel6, answersQuestion6, greatAnswer6, answerDetail6),
                new Question(7, questionLabel7, answersQuestion7, greatAnswer7, answerDetail7),
                new Question(8, questionLabel8, answersQuestion8, greatAnswer8, answerDetail8),
                new Question(9, questionLabel9, answersQuestion9, greatAnswer9, answerDetail9),
                new Question(10, questionLabel10, answersQuestion10, greatAnswer10, answerDetail10)
        );

        String mcqName = "=== QCM Java - Les collections - partie 1";

        return new Mcq(2, mcqName, questionsMcq2);
    }
}
