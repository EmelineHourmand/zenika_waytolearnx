package fr.zenika.domaine;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class GlobalMcq {

    private static List<Mcq> mcqs = new ArrayList<>();

    /**
     * Permet d'ajouter un QCM dans la liste globale des QCM.
     * @param mcq est un QCM.
     */
    public void addMcqToGlobalMcq(Mcq mcq) {
        mcqs.add(mcq);
    }

    /**
     * Permet de récupérer la liste des QCM.
     * @return la liste des QCM.
     */
    public List<Mcq> getMcq() {
        return mcqs;
    }
}
