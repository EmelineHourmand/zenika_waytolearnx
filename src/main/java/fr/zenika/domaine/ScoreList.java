package fr.zenika.domaine;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class ScoreList {

    private static List<Score> scores = new ArrayList<>();

    /**
     * Permet d'ajouter un score dans la liste des scores.
     * @param score un score à ajouter.
     */
    public void addScoreToScoreList(Score score) {
        scores.add(score);
    }

    /**
     * Permet de récupérer la liste des scores.
     * @return la liste des scores.
     */
    public List<Score> getScores() {
        return scores;
    }

    /**
     * Permet d'ajouter une liste de score.
     * @param scores liste de score.
     */
    public void setScores(List<Score> scores) {
        ScoreList.scores = scores;
    }
}
