package fr.zenika.domaine;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class User {

    private String name;
    private int score;
    private List<Question> falseQuestion;
    private List<String> falseAnswer;
    private LocalDateTime dateMcq;

    /**
     * Constructeur d'un utilisateur.
     * @param name nom de l'utilisateur.
     * @param score score de l'utilisateur.
     * @param falseQuestion Liste des questions auxquels il eut faux.
     * @param falseAnswer Liste des réponses aux questions auxquels il a eut faux.
     * @param dateMcq la date de la réalisation du QCM.
     */
    public User(String name, int score, List<Question> falseQuestion,
                List<String> falseAnswer, LocalDateTime dateMcq) {
        this.name = name;
        this.score = score;
        this.falseQuestion = falseQuestion;
        this.falseAnswer = falseAnswer;
        this.dateMcq = dateMcq;
    }

    /**
     * Récupère le nom de l'utilisateur.
     * @return le nom de l'utilisateur.
     */
    public String getName() {
        return name;
    }

    /**
     * Permet de renseigner le nom de l'utilisateur.
     * @param name nom de l'utilisateur.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Permet de récupérer le score de l'utilisateur.
     * @return le score de l'utilisateur.
     */
    public int getScore() {
        return score;
    }

    /**
     * Permet de renseigner le score de l'utilisateur.
     * @param score le score de l'utilisateur.
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Permet de récupérer la liste des questions auxquels l'utilisateur a eus faux.
     * @return la liste des questions auxquels l'utilisateur a eus faux.
     */
    public List<Question> getFalseQuestion() {
        return falseQuestion;
    }

    /**
     * Permet d'ajouter une question dans la liste des questions auxquels l'utilisateur a eus faux.
     * @param falseQuestion Une question.
     */
    public void setFalseQuestion(Question falseQuestion) {
        this.falseQuestion.add(falseQuestion);
    }

    /**
     * Permet de récupérer les réponses fausses de l'utilisateur.
     * @return les réponses fausses de l'utilisateur.
     */
    public List<String> getFalseAnswer() {
        return falseAnswer;
    }

    /**
     * Permet d'ajouter une réponse fausse a la liste des réponses fausses de l'utilisateur.
     * @param falseAnswer réponse fausse.
     */
    public void setFalseAnswer(String falseAnswer) {
        this.falseAnswer.add(falseAnswer);
    }

    /**
     * Permet de récupérer la date à laquelle l'utilisateur a remplis le QCM.
     * @return la date à laquelle l'utilisateur a remplis le QCM.
     */
    public LocalDateTime getDateMcq() {
        return dateMcq;
    }

    /**
     * Permet de définir à quelle date l'utilisateur a remplis le QCM.
     * @param dateMcq la date où le QCM a été remplis.
     */
    public void setDateMcq(LocalDateTime dateMcq) {
        this.dateMcq = dateMcq;
    }
}
