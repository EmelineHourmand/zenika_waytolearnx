package fr.zenika.domaine;

/**
 * @author Emeline Hourmand
 */
public class Answer {

    private final char answerId;
    private final String answerLabel;

    /**
     * Constructeur de réponses.
     * @param answerId l'ID de la réponse en CHAR.
     * @param answerLabel le text de la réponse.
     */
    public Answer(char answerId, String answerLabel) {
        this.answerId = answerId;
        this.answerLabel = answerLabel;
    }

    /**
     * Permet de récuperer l'ID de la réponse.
     * @return id de la réponse.
     */
    public char getAnswerId() {
        return answerId;
    }

    /**
     * Permet de récupérer le texte de la réponse.
     * @return le text de la réponse.
     */
    public String getAnswerLabel() {
        return answerLabel;
    }
}
