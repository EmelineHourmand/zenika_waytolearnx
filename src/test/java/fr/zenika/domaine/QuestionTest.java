package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class QuestionTest {

    List<Answer> answers = List.of(
            new Answer('A', "La réponse A."),
            new Answer('B', "La réponse B."),
            new Answer('C', "La réponse C.")
    );
    String questionLabel = "Quelle est la bonne réponse ?";
    Set<String> greatAnswers = new HashSet<>();
    String answerDetail = "Détails de la réponse.";
    Question question = new Question(1, questionLabel, answers, greatAnswers, answerDetail);

    @Test
    void get_id_question() {
        assertEquals(1, question.getQuestionId());
    }

    @Test
    void get_answers() {
        assertEquals(answers, question.getAnswers());
    }

    @Test
    void get_question_label() {
        assertEquals(questionLabel, question.getQuestionLabel());
    }

    @Test
    void get_great_answers_list() {
        greatAnswers.add("A");
        assertEquals(greatAnswers, question.getGreatAnswer());
    }

    @Test
    void get_answer_detail() {
        assertEquals(answerDetail, question.getAnswerDetails());
    }
}