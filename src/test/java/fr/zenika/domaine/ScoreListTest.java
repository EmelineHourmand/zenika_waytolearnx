package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScoreListTest {

    Score score1 = new Score(1, new ArrayList<>());
    List<Score> scores = List.of(
            score1
    );
    ScoreList scoreList = new ScoreList();

    @Test
    void add_score_into_the_score_list() {
        scoreList.setScores(new ArrayList<>());
        scoreList.addScoreToScoreList(score1);
        assertEquals(scores.get(0), scoreList.getScores().get(0));
    }

    @Test
    void get_score_list() {
        scoreList.setScores(scores);
        assertEquals(scores, scoreList.getScores());
    }

    @Test
    void set_score_list() {
        scoreList.setScores(scores);
        assertEquals(scores, scoreList.getScores());
    }
}