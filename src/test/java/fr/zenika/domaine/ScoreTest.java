package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ScoreTest {

    List<Answer> answers = List.of(
            new Answer('A', "La réponse A."),
            new Answer('B', "La réponse B."),
            new Answer('C', "La réponse C.")
    );
    String questionLabel = "Quelle est la bonne réponse ?";
    Set<String> greatAnswers = new HashSet<>();
    String answerDetail = "Détails de la réponse.";
    Question question = new Question(1, questionLabel, answers, greatAnswers, answerDetail);
    List<Question> questions = List.of (
            question
    );

    User user = new User("Toto", 0, questions, new ArrayList<>(), LocalDateTime.now());
    Score score = new Score(1, new ArrayList<>());

    @Test
    void get_id_score() {
        assertEquals(1, score.getScoreId());
    }

    @Test
    void set_id_score() {
        score.setScoreId(2);
        assertEquals(2, score.getScoreId());
    }

    @Test
    void get_users() {
        score.setUsers(user);
        assertEquals(user, score.getUsers().get(0));
    }

    @Test
    void set_users() {
        score.setUsers(user);
        assertEquals(user, score.getUsers().get(0));
    }
}