package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnswerTest {

    String answerLabel = "La réponse A";
    Answer answer = new Answer('A', answerLabel);

    @Test
    void get_id_answer() {
        assertEquals('A', answer.getAnswerId());
    }

    @Test
    void get_answer_label() {
        assertEquals(answerLabel, answer.getAnswerLabel());
    }
}
