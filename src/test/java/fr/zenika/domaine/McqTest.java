package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class McqTest {

    List<Answer> answers = List.of(
            new Answer('A', "La réponse A."),
            new Answer('B', "La réponse B."),
            new Answer('C', "La réponse C.")
    );
    String questionLabel = "Quelle est la bonne réponse ?";
    Set<String> greatAnswers = new HashSet<>();
    String answerDetail = "Détails de la réponse";
    List<Question> questions = List.of(
            new Question(1, questionLabel, answers, greatAnswers, answerDetail),
            new Question(2, questionLabel, answers, greatAnswers, answerDetail),
            new Question(3, questionLabel, answers, greatAnswers, answerDetail)
    );

    Mcq mcq = new Mcq(1, "MCQ 1", questions);

    @Test
    void get_mcq_id() {
        assertEquals(1, mcq.getMcqId());
    }

    @Test
    void get_questions() {
        assertEquals(questions, mcq.getQuestions());
    }
}