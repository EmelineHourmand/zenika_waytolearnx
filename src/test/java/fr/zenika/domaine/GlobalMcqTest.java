package fr.zenika.domaine;

import fr.zenika.domaine.service.GlobalMcqManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GlobalMcqTest {

    List<Answer> answers = List.of(
            new Answer('A', "La réponse A."),
            new Answer('B', "La réponse B."),
            new Answer('C', "La réponse C.")
    );
    String questionLabel = "Quelle est la bonne réponse ?";
    Set<String> greatAnswers = new HashSet<>();;
    String answerDetail = "Détails de la réponse";
    List<Question> questions = List.of(
            new Question(1, questionLabel, answers, greatAnswers, answerDetail),
            new Question(2, questionLabel, answers, greatAnswers, answerDetail),
            new Question(3, questionLabel, answers, greatAnswers, answerDetail)
    );

    GlobalMcq globalMcq = GlobalMcqManager.getInstance().getGlobalMcq();
    Mcq mcq = new Mcq(1, "MCQ 1", questions);
    List<Mcq> mcqList = globalMcq.getMcq();

    @Test
    void add_mcq() {
        globalMcq.addMcqToGlobalMcq(mcq);
        assertEquals(mcq, globalMcq.getMcq().get(0));
    }

    @Test
    void get_mcq() {
        System.out.println(mcqList);
        assertEquals(mcqList, globalMcq.getMcq());
    }
}