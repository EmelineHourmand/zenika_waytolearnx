package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    List<Answer> answers = List.of(
            new Answer('A', "La réponse A."),
            new Answer('B', "La réponse B."),
            new Answer('C', "La réponse C.")
    );
    String questionLabel = "Quelle est la bonne réponse ?";
    Set<String> greatAnswers = new HashSet<>();
    String answerDetail = "Détails de la réponse.";
    Question question = new Question(1, questionLabel, answers, greatAnswers, answerDetail);
    List<Question> questions = new ArrayList<>();

    User user = new User("Toto", 0, questions, new ArrayList<>(), LocalDateTime.now());

    @Test
    void get_user_name() {
        assertEquals("Toto", user.getName());
    }

    @Test
    void set_user_name() {
        user.setName("Tata");
        assertEquals("Tata", user.getName());
    }

    @Test
    void get_user_score() {
        assertEquals(0, user.getScore());
    }

    @Test
    void set_user_score() {
        user.setScore(2);
        assertEquals(2, user.getScore());
    }

    @Test
    void set_false_question() {
        user.setFalseQuestion(question);
        assertEquals(question, user.getFalseQuestion().get(0));
    }

    @Test
    void get_false_questions() {
        user.setFalseQuestion(question);
        assertEquals(questions, user.getFalseQuestion());
    }
}